use ansi_term::Color;
use regex::Regex;
use std::{
    env,
    error::Error,
    fs,
    io::{self, BufRead},
};

pub struct Config {
    regex: Regex,
    filename: Option<String>,
}

impl Config {
    pub fn new(mut args: env::Args) -> Result<Config, &'static str> {
        args.next();

        let regex = match args.next() {
            Some(arg) => arg,
            None => return Err("Expected at least 1 argument, got 0."),
        };
        let regex = match Regex::new(&regex) {
            Ok(regex) => regex,
            Err(_) => return Err("Regex could not be parsed."),
        };

        let filename = args.next();

        if args.next().is_some() {
            return Err("Expected at most 2 arguments.");
        }

        Ok(Config { regex, filename })
    }
}

pub fn run(config: Config) -> Result<bool, Box<dyn Error>> {
    let mut zero_results = true;

    match config.filename {
        None => {
            for line in io::stdin().lock().lines() {
                let line = line.expect("Could not read line from stdin.");

                if color_regex_matches_print(&config.regex, &line) && zero_results {
                    zero_results = false;
                }
            }
        }
        Some(filename) => {
            let contents = fs::read_to_string(filename)?;

            for line in contents.lines() {
                if color_regex_matches_print(&config.regex, line) && zero_results {
                    zero_results = false;
                }
            }
        }
    }

    Ok(zero_results)
}

fn color_regex_matches_print(regex: &Regex, line: &str) -> bool {
    match color_regex_matches(regex, line) {
        Some(colored_line) => {
            println!("{}", colored_line);
            true
        }
        None => false,
    }
}

fn color_regex_matches(regex: &Regex, line: &str) -> Option<String> {
    if regex.is_match(line) {
        let mut result = line.to_string();
        let style = Color::Red.bold();
        let prefix = style.prefix().to_string();
        let suffix = style.suffix().to_string();
        let prefix_len = prefix.len();
        let prefix_and_suffix_len = prefix_len + suffix.len();
        for (i, regex_match) in regex.find_iter(line).enumerate() {
            result.insert_str(regex_match.start() + i * prefix_and_suffix_len, &prefix);
            result.insert_str(
                regex_match.end() + i * prefix_and_suffix_len + prefix_len,
                &suffix,
            );
        }
        return Some(result);
    }
    None
}
