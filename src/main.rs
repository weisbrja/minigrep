use minigrep::Config;
use std::{env, process};

fn main() {
    let config = Config::new(env::args()).unwrap_or_else(|err| {
        eprintln!("{}", err);
        process::exit(2);
    });

    let zero_results = minigrep::run(config);
    match zero_results {
        Err(e) => {
            eprintln!("Application error: {}", e);
            process::exit(2);
        }
        Ok(true) => process::exit(1),
        Ok(false) => (),
    }
}
